sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"twobm/sap/experience/model/models"
], function(UIComponent, Device, models, googlemaps) {
	"use strict";
	return UIComponent.extend("twobm.sap.experience.Component", {
		metadata: {
			manifest: "json"
		},

		init: function() {
			// from http://jasper07.secondphase.com.au/openui5-googlemaps/
			// Replace API key with SAP Denmark API KEY
			window.GMAPS_API_KEY = "AIzaSyAUQ181s7XXQFBnnUQwNs5Gj6Ywu7Fcksc";
			sap.ui.getCore().loadLibrary("openui5.googlemaps", "./openui5/googlemaps");
			
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// set the device model
			this.setModel(models.createDeviceModel(), "device");

			this.getRouter().initialize();

			if (sap.hybrid) {
				// Configure status bar
				if (window.cordova.require("cordova/platform").id === "ios") {
					StatusBar.backgroundColorByName("white");
					StatusBar.styleDefault();
					StatusBar.overlaysWebView(false);
				}
			}
		}
	});
});