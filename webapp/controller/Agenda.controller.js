sap.ui.define([
	"twobm/sap/experience/controller/BaseController",
	"twobm/sap/experience/model/SAPExperienceModels",
	"sap/ui/core/UIComponent",
	'jquery.sap.global',
	'sap/ui/model/json/JSONModel'
	
], function(BaseController, SAPExperienceModels, UIComponent, JSONModel) {
	"use strict";

	return BaseController.extend("twobm.sap.experience.controller.Agenda", {
		onInit: function() {
			var jsonModel = new sap.ui.model.json.JSONModel();
			jsonModel.loadData("./data/items.json");
			this.getView().setModel(jsonModel);
			var visitDetailModel = SAPExperienceModels.getVisitDetailsModel("1", "PARIS");
			this.getView().setModel(visitDetailModel, "VisitModel");
		},

		onIconPress: function(oEvent) {
			this.getRouter().navTo(oEvent.getSource().getAlt());
		},

		onItemPress: function(oEvent) {
			this.getRouter().navTo("agendaDetail", {
				agendaContext: oEvent.getSource().getBindingContext().getPath().substr(7)
			});
		}
	});
});