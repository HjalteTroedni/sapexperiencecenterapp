sap.ui.define([
	"twobm/sap/experience/controller/BaseController",
	"sap/ui/core/UIComponent",
	'jquery.sap.global',
	'sap/ui/model/json/JSONModel'
], function(BaseController, UIComponent, JSONModel) {
	"use strict";

	return BaseController.extend("twobm.sap.experience.controller.Summary", {
		onInit: function() {
			// var directions = new openui5.googlemaps.Directions({
			// 	startAddress: "Kongensvej 31, 2000",
			// 	endAddress: "Vesterbrogade 91A, 1620",
			// 	travelMode: openui5.googlemaps.TravelMode.transit
			// });

			var jsonModel = new sap.ui.model.json.JSONModel();
			jsonModel.loadData("./data/items.json");
			this.getView().setModel(jsonModel);

			//var contextPath = "/Places";
			// var givenContext = new sap.ui.model.Context(this.getView().getModel(), contextPath);

			// this.getView().setBindingContext(givenContext);
			// this.getView().bindElement(contextPath);
		},
		onIconPress: function(oEvent) {
			this.getRouter().navTo(oEvent.getSource().getAlt());
		}
	});
});