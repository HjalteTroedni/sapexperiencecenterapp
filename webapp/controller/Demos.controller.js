sap.ui.define([
	"twobm/sap/experience/controller/BaseController",
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/UIComponent"
], function(BaseController, UIComponent) {
	"use strict";

	return BaseController.extend("twobm.sap.experience.controller.Demos", {
		onInit: function() {
			var jsonModel = new sap.ui.model.json.JSONModel();
			jsonModel.loadData("./data/items.json");
			this.getView().setModel(jsonModel);
		},

		onIconPress: function(oEvent) {
			this.getRouter().navTo(oEvent.getSource().getAlt());
		},

		itemFactory: function(sId, oContext) {
			var obj = oContext.getObject();

			var newsContent = new sap.m.NewsContent();
			newsContent.setContentText(obj.Description);
			newsContent.setSubheader(obj.Title);

			var tileContent = new sap.m.TileContent();
			tileContent.setContent(newsContent);
			tileContent.setFooter(obj.Company);

			var genericTile = new sap.m.GenericTile();
			genericTile.setFrameType(sap.m.FrameType.TwoByOne);
			genericTile.setBackgroundImage(obj.Img);
			genericTile.addTileContent(tileContent);
			genericTile.attachPress(function(oEvent) {
				this.onItemPress(oEvent);
			}.bind(this));

			var tile = new sap.m.SlideTile();
			tile.addStyleClass("sapUiTinyMarginBegin");
			tile.addStyleClass("sapUiTinyMarginTop");
			tile.setTransitionTime(250);
			tile.setDisplayTime(2500);
			tile.addTile(genericTile);

			return tile;
		},

		onItemPress: function(oEvent) {
			this.getRouter().navTo("demoDetail", {
				demoContext: oEvent.getSource().getBindingContext().getPath().substr(7)
			});
		},

	});
});