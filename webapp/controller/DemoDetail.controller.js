sap.ui.define([
	'twobm/sap/experience/controller/BaseController',
	'sap/ui/core/UIComponent',
	'jquery.sap.global',
	'sap/ui/model/json/JSONModel'
], function(BaseController, UIComponent, JSONModel, History) {
	"use strict";

	return BaseController.extend("twobm.sap.experience.controller.DemoDetail", {
		onInit: function() {
			// Register to the detail route matched
			this.getRouter().getRoute("demoDetail").attachPatternMatched(this.onRouteMatched, this);
		},

		onRouteMatched: function(oEvent) {
			var oArguments = oEvent.getParameter("arguments");
			var contextPath = '/Demos/' + oArguments.demoContext;
			var givenContext = new sap.ui.model.Context(this.getView().getModel(), contextPath);

			this.getView().setBindingContext(givenContext);
			this.getView().bindElement(contextPath);
		},
		
		onNavBack: function() {
				this.getRouter().navTo("demos");
		}
	});
});