sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/odata/v2/ODataModel"
], function(JSONModel, ODataModel) {
	"use strict";

	return {

		// VisitorID: Int ("1")
		// Location: String ("PARIS")
		getVisitDetailsModel: function(visitorID, location) {
			var oModel = new JSONModel();
			var url = "/sapExperienceApp/get_Visits_Details.xsjs?visitId=" + visitorID + "&location=" + location;
			$.ajax({
				type: 'GET',
				url: url,
				headers: {},
				async: false
			}).done(function(results) {
				if(!results) { return; }
				oModel.setProperty("/Company", results.Company);
				oModel.setProperty("/Comment", results.Comment);
				oModel.setProperty("/NumberOfParticipants", results.NumberOfParticipants);
				oModel.setProperty("/StartDate", results.StartDate);
				oModel.setProperty("/EndDate", results.EndDate);
				oModel.setProperty("/VisitName", results.VisitName);
				oModel.setProperty("/NameofMeeting", results.NameofMeeting);
				oModel.setProperty("/TypeOfMeeting", results.TypeOfMeeting);
				oModel.setProperty("/Visitors", results.Visitors);
				oModel.setProperty("/Host", results.Host);
				oModel.setProperty("/Agenda", results.Agenda);
			}).fail(function(err) {
				if (err !== undefined) {
					var oErrorResponse = $.parseJSON(err.responseText);
					sap.m.MessageToast.show(oErrorResponse.message, {
						duration: 6000
					});
				} else {
					sap.m.MessageToast.show("Unknown error!");
				}
			});
			return oModel;
		}
	};
});